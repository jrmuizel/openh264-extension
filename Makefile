RELEASE_KIND?=beta
ARCH?=$(shell uname -m | sed "s/^i.86$$/i686/" | sed "s/^ppc/powerpc/")
REPO?=repo
ARCH_OPTS=-o arch $(ARCH)
BST=bst --colors $(ARCH_OPTS) -o release_kind $(RELEASE_KIND)
VERSION=2.0
ifeq ($(RELEASE_KIND), beta)
BRANCH=$(VERSION)beta
else
BRANCH=$(VERSION)
endif

build: elements
	$(BST) build flatpak-repo.bst test-extension.bst

clean-repo:
	rm -rf $(REPO)

export-repo: clean-repo build
	$(BST) checkout flatpak-repo.bst $(REPO)


test-extension: export XDG_DATA_HOME=$(CURDIR)/temp
test-extension: export-repo
	rm -rf temp
	mkdir -p temp
	flatpak remote-add --if-not-exists --user --no-gpg-verify openh264 $(REPO)
	flatpak install --user -v -y openh264 org.freedesktop.Platform.openh264//$(BRANCH)
	$(BST) shell --mount ${XDG_DATA_HOME}/flatpak/runtime/org.freedesktop.Platform.openh264/$(ARCH)/$(BRANCH)/active/files/extra/ /flatpak \
	--mount /etc/resolv.conf /etc/resolv.conf test-extension.bst /bin/test-extension.sh


.PHONY: build clean-repo export test-extension
