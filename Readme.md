Flatpak OpenH264 extension
===========================
This extension provides bootstrapping mechanism which allows installing Cisco's
free to use OpenH264 codec on end-user machine. This system is intended to be
used in conjunction with noopenh264. Applications link against stubs in noopenh264
and as such it determines the ABI. Applications however use openh264 during
runtime so the two must be ABI-compatible.

Sanity-checking installation
----------------------------
You can run `make test-extension` to run brief check that the extension downloads
Cisco openh264 as extended and the output is not corrupt

Testing OpenH264
----------------
The release version of openh264 is installed by default. You will need to uninstall
it before testing the beta version. You can either install beta version from Flathub
beta or use following to generate it locally
```
make export-repo
flatpak remote-add --if-not-exists --user --no-gpg-verify openh264 repo
flatpak install --user -v -y openh264 org.freedesktop.Platform.openh264
```
After this the extension should be loaded on next restart of an application. You can
verify from /.flatpak-info inside application sandbox that correct openh264 extension
is enabled.

