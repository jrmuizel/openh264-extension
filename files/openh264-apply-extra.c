#include <elf.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <bzlib.h>
#include <fcntl.h>
#include <unistd.h>
#include "config.h"


FILE*
safe_open (const char *filename, const char *mode)
{
  FILE *f = fopen (filename, mode);
  if (f == NULL)
  {
    fprintf (stderr, "Failed to open file %s\n", filename);
    exit (1);
  }

  return f;
}


void
extract (BZFILE *source, FILE *target)
{
  const int BUF_SIZE = 64 * 1024;
  int bzerror = BZ_OK;
  size_t bytes_read = BUF_SIZE;
  char buffer[BUF_SIZE];

  do
  {
    bytes_read = BZ2_bzRead (&bzerror, source, buffer, BUF_SIZE);
    if (bzerror == BZ_OK || bzerror == BZ_STREAM_END)
    {
      size_t bytes_written = fwrite (buffer, 1, bytes_read, target);

      if (bytes_written != bytes_read)
      {
        fprintf (stderr,
                 "Short Write: read %zu bytes, but only wrote %zu bytes\n",
                 bytes_read, bytes_written);
        exit (2);
      }
    }
    else
    {
      fprintf (stderr, "Failed to read bzip2 stream: libbzip2 error %d\n",
               bzerror);
      exit (3);
    }
  }
  while (bzerror == BZ_OK);
}

void sorename(FILE* file, const char* new_name)
{
  Elf64_Ehdr header;
  Elf64_Shdr* section_headers;
  size_t n_entries, j;
  Elf64_Dyn* entries;
  int found = 0;
  int i;

  if (fseek(file, 0, SEEK_SET) != 0) {
    perror("Cannot seek ELF header");
    exit (4);
  }
  if (fread(&header, sizeof(Elf64_Ehdr), 1, file) != 1) {
    perror("Cannot read ELF header");
    exit (4);
  }

  if (header.e_shentsize != sizeof(Elf64_Shdr)) {
    fprintf (stderr, "Unexpected section header size");
    exit (4);
  }

  if (fseek(file, header.e_shoff, SEEK_SET) != 0) {
    perror("Cannot seek section headers");
    exit (4);
  }

  section_headers = malloc(sizeof(Elf64_Shdr)*header.e_shnum);
  if (section_headers == NULL) {
    perror("Cannot allocate buffer for section headers");
    exit (4);
  }

  if (fread(section_headers, sizeof(Elf64_Ehdr), header.e_shnum, file) != header.e_shnum) {
    perror("Cannot read section headers");
    exit (4);
  }

  for (i = 0; i < header.e_shnum; ++i) {
    if (section_headers[i].sh_type == SHT_DYNAMIC)
      break ;
  }
  if (i >= header.e_shnum) {
    fprintf (stderr, "No dynamic section found");
    exit (4);
  }

  if (fseek(file, section_headers[i].sh_offset, SEEK_SET) != 0) {
    perror("Cannot seek dynamic section");
    exit (4);
  }

  n_entries = section_headers[i].sh_size/sizeof(Elf64_Dyn);
  entries = malloc(n_entries*sizeof(Elf64_Dyn));
  if (entries == NULL) {
    perror("Cannot allocate buffer for dynamic section");
    exit (4);
  }

  if (fread(entries, sizeof(Elf64_Dyn), n_entries, file) != n_entries) {
    perror("Cannot read dynamic section");
    exit (4);
  }

  size_t strtab_start = SIZE_MAX;
  size_t strtab_size = SIZE_MAX;
  for (j = 0; j < n_entries; ++j) {
    if (entries[j].d_tag == DT_STRTAB) {
      strtab_start = entries[j].d_un.d_ptr;
    }
    if (entries[j].d_tag == DT_STRSZ) {
      strtab_size = entries[j].d_un.d_val;
    }
    if ((strtab_size != SIZE_MAX) &&
        (strtab_start != SIZE_MAX)) {
      break ;
    }
  }

  if ((strtab_start == SIZE_MAX) || (strtab_size == SIZE_MAX)) {
    fprintf(stderr, "String table not found\n");
    exit(4);
  }

  char* string_table = malloc(strtab_size);
  if (string_table == NULL) {
    perror("Cannot allocate string table");
    exit(4);
  }

  if (fseek(file, strtab_start, SEEK_SET) != 0) {
    perror("Cannot seek to string table");
    exit(4);
  }

  if (fread(string_table, sizeof(char), strtab_size, file) != strtab_size) {
    perror("Cannot read string table");
    exit(4);
  }

  for (j = 0; j < n_entries; ++j) {
    if (entries[j].d_tag == DT_SONAME) {
      if (entries[j].d_un.d_val > strtab_size) {
        fprintf(stderr, "String out of table\n");
        exit(4);
      }
      char *old_soname = string_table + entries[j].d_un.d_val;
      size_t new_size = strlen(new_name);
      size_t old_size = strnlen(old_soname,
                                strtab_size - entries[j].d_un.d_val);
      if (old_size < new_size) {
        fprintf (stderr, "New soname is too long");
        exit(4);
      }
      strcpy(old_soname, new_name);
      ++found;
      break ;
    }
  }

  if (!found) {
    fprintf (stderr, "Found no soname");
    exit(4);
  }

  if (fseek(file, strtab_start, SEEK_SET) != 0) {
    perror("Cannot seek to string table");
    exit(4);
  }

  if (fwrite(string_table, sizeof(char), strtab_size, file) != strtab_size) {
    perror("Cannot write string table");
    exit(4);
  }

  free(string_table);
  free(entries);
  free(section_headers);
}


void
unpack_extradata (extra_data_t *extra_data)
{
  FILE *archive;
  FILE *library;
  int bzerror = BZ_OK;

  archive = safe_open (extra_data->archive, "rb");
  library = safe_open (extra_data->filename, "wb+");

  BZFILE *bzfile = BZ2_bzReadOpen (&bzerror, archive, 0, 0, NULL, 0);
  if (bzerror != BZ_OK)
  {
    fprintf (stderr, "Failed to open bzip archive %s: libbzip2 error %d\n",
             extra_data->archive, bzerror);
    BZ2_bzReadClose (&bzerror, bzfile);
    exit (4);
  }

  extract (bzfile, library);

  BZ2_bzReadClose(&bzerror, bzfile);
  if (bzerror != BZ_OK)
  {
    fprintf (stderr, "Failed to close: libbzip2 error %d\n", bzerror);
    exit (6);
  }

  fclose (archive);

  if (extra_data->soname) {
    sorename(library, extra_data->soname);
  }

  fclose (library);
  if(extra_data->soname != NULL)
    {
      symlink(extra_data->filename, extra_data->soname);
    }
  unlink (extra_data->archive);

}


int main(void)
{
  for(size_t i = 0; i < sizeof(extra_datas)/sizeof(extra_data_t); i++)
    {
      unpack_extradata(&extra_datas[i]);
    }
  return 0;
}
